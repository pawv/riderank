# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170125101329) do

  create_table "rides", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "taxi_provider_id"
    t.decimal  "price",            precision: 10, scale: 2
    t.decimal  "distance",         precision: 10, scale: 2
    t.string   "origin"
    t.string   "destination"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "rides", ["taxi_provider_id"], name: "index_rides_on_taxi_provider_id"
  add_index "rides", ["user_id"], name: "index_rides_on_user_id"

  create_table "taxi_providers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
