class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.references :user, index: true, foreign_key: true
      t.references :taxi_provider, index: true, foreign_key: true
      t.decimal :price, precision: 10, scale: 2
      t.decimal :distance, precision: 10, scale: 2
      t.string :origin
      t.string :destination

      t.timestamps null: false
    end
  end
end
