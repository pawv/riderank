class Ride < ActiveRecord::Base
  belongs_to :user
  belongs_to :taxi_provider

  validates :taxi_provider, presence: { message: 'Taxi provider can\'t by blank.' }
  validates :price, presence: true, numericality: { greater_than: 0, less_than: 10000 }
  validates :distance, presence: true, numericality: { greater_than: 0, less_than: 10000 }

  validates :origin, presence: true, location_format: { message: 'origin location has invalid format.' }
  validates :destination, presence: true, location_format: { message: 'destination location has invalid format.' }

  before_validation :calculate_distance

  scope :stats_from, -> (date) { select('rides.created_at, sum(rides.distance) as distance_sum, avg(rides.distance) as distance_avg, avg(rides.price) as price_avg, group_concat(DISTINCT taxi_providers.name) as taxi_provider_names')
  									.joins(:taxi_provider)
  									.where(created_at: date..Time.zone.now.end_of_day).group('date(rides.created_at)') }

  # TODO
  # change callback + move to sidekiq if performance needed
  def calculate_distance
  	route = Route.new(origin: origin, destination: destination)
  	self.distance = route.distance.to_f
  end

end
