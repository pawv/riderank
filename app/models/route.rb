class Route

	attr_accessor :origin, :destination

	API_KEY = 'AIzaSyCMhZ4FD0FSENSoUumNgosUTbDfYNtmvN0'

	def initialize(options = {})
		self.origin = options[:origin]
		self.destination = options[:destination]
	end

	def results
		require 'rest-client'
		url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=#{API_KEY}"
		response = RestClient.get url, { params: { origins: origin, destinations: destination } }
		JSON.parse(response.body)
	end

	def distance
		data = results
		data['rows'].first['elements'].first['distance']['text']
	rescue
		raise 'Distance calculation fails'
	end

end
