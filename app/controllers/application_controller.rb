class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
  	@current_user ||= User.find_or_create_by(token: token)
  end

  def token
  	return cookies[:token] unless cookies[:token].blank?
  	require 'securerandom'
  	loop do
  		generated_token = SecureRandom.hex
  		next if User.exists?(token: generated_token)
  		cookies[:token] = generated_token
  		break
  	end
  	cookies[:token]
  end

end
