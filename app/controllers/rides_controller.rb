class RidesController < ApplicationController

	def index
		@rides = current_user.rides.stats_from(Date.today.at_beginning_of_month)
	end

	def new
		@ride = Ride.new
	end

	def create
		@ride = Ride.new(ride_params)
		if @ride.valid? && @ride.save
			redirect_to new_ride_path, flash: { notice: 'Ride successfully added.' }
		else
			render :new
		end
	end

	private

	def ride_params
		params.require(:ride)
			  .permit(:taxi_provider_id, :price, :origin, :destination)
			  .merge(user_id: current_user.id)
	end


end
