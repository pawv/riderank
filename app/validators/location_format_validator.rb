class LocationFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^,]+),([^,]+),([^,]+)\z/i
      record.errors[attribute] << (options[:message] || 'Wrong format. Should by like "Street+Number, City, Country"')
    end
  end
end
